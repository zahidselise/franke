const API_TOKEN_KEY = 'franke_secure_vault_token';

export const getToken = () => {
    return window.localStorage.getItem(API_TOKEN_KEY)
};

export const setToken = token => {
    window.localStorage.setItem(API_TOKEN_KEY, token);
};

export const unsetToken = () => {
    window.localStorage.removeItem(API_TOKEN_KEY);
};

// export const setUser = (user) {
//     window.localStorage = 
// };

export default {getToken, setToken, unsetToken}