import Vue from 'vue'
import App from './pages/App.vue'
import router from './router';
import store from './store';

import CKEditor from 'ckeditor4-vue/dist/legacy.js';
import vuetify from './plugins/vuetify';
import Vuelidate from 'vuelidate'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
Vue.use( CKEditor );
Vue.use(Vuelidate);
Vue.config.productionTip = false

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
