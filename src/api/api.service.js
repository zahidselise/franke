import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

import JwtService from "../common/jwt.service";
import {API_URL} from "../common/config";

Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = API_URL;

const ApiService = {
    init(){
    },

    setHeader(){
        // Vue.axios.defaults.headers.common = {
        //     //'X-Requested-With': 'XMLHttpRequest',
        //     "Authorization": 'Bearer ' + JwtService.getToken(),
        //     "content-type": 'application/json'
        // };
        Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + JwtService.getToken();
        Vue.axios.defaults.headers.common['Content-Type'] = 'application/json';
        // console.log(Vue.axios.defaults.headers.common);
    },

    delete(resource, slug ="") {
        return Vue.axios.delete(`${resource}/${slug}`).catch(error => {
            throw new Error(`ApiService ${error}`);
        });
    },

    get(resource, slug = "") {
        // return new Promise((resolve, reject) => {
        //     // debugger;
        //     var url = `${API_URL}/${resource}/${slug}`;
        //     var authorization = 'Bearer ' + JwtService.getToken();
        //     var xhttp = new XMLHttpRequest();
        //     xhttp.onreadystatechange = () => {
        //         if (xhttp.readyState == 4) {

        //             if (xhttp.status === 200) {
        //                 let res = {};
        //                 res.data = JSON.parse(xhttp.responseText);
        //                 resolve(res);
        //             } else {
        //                 let res = {};
        //                 res.response = JSON.parse(xhttp.responseText);
        //                 reject(res);
        //             }
        //         } 
        //     };
        //     xhttp.open("GET", url, true);
        //     xhttp.setRequestHeader("Authorization", authorization);
        //     xhttp.setRequestHeader("Content-Type", 'application/json');
        //     xhttp.send();
        //     // return xhttp.responseText;
        // });
        // return Vue.axios.get(`${resource}/${slug}`).catch(error => {
        //     throw new Error(`ApiService ${error}`);
        // });

        let config = {
            headers: {}
        };


        config.headers.Authorization = 'Bearer '+JwtService.getToken();
        axios.get(`${API_URL}${resource}/${slug}`, config).then((res) => {
            console.log(res);
        });
    },

    post(resource, params) {
        return Vue.axios.post(`${resource}`, params);
    },
};

export default ApiService;