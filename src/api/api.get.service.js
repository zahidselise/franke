import axios from "axios";

import JwtService from "../common/jwt.service";
import {API_URL} from "../common/config";

export const fetchData = (resource, slug = "") => {
	let config = {
	    headers: {}
	};


	config.headers.Authorization = 'Bearer '+JwtService.getToken();
	return axios.get(`${API_URL}${resource}/${slug}`, config).then((res) => {
	    console.log(res);
	});
}