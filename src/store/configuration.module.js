import axios from "axios"; 
const state = {
      PROD_SYS:0,
      PROD_SYS_Filtered:0,
      flavourStation:0,
      FLAVOR_ST01:0,
      FLAVOR_ST01_Filtered:[],
      
      loading:false,
      coffeeMachine:0,
      COFFEE_MC01:0,
      
      COFFEE_MC01_Filtered:[],

      coolingUnit:0,
      COOLINGUNIT:0,
      COFFEE_MC_ID:0,
      COOLINGUNIT_ID:0,
      COOLINGUNIT_Filtered:[],

      // PIM:{
      //   sap_nr:'A200 FM',
      //   att_url:'https://onepim-content.franke.com/download/GetAttachmentImage/223081?idFile=ZPP_001_A200-FM_nd1000w.jpg',
      //   att_category_text:'Product Pictures',
      //   webtext:'<p>Fully automatic coffee machine with FoamMaster milk system</p><p><span>2 coffee grinders</span><br/></p><p><span>Built-in water tank or fixed water connection</span><br/></p>',
      //   attr_name:'ATT-CS-MAN-RISTRETTO',
      //   attr_key:'Ristretto',
      //   attr_value:'yes'
      // },

    PIM:[
      {
        ProductName:'A200 FM',
        ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/223081?idFile=ZPP_001_A200-FM_nd1000w.jpg',
        ProductDescription:'<p>Fully automatic coffee machine with FoamMaster milk system</p><p><span>2 coffee grinders</span><br/></p><p><span>Built-in water tank or fixed water connection</span><br/></p>',
        BeverageCreations:[
          {attr_key:'Ristretto',attr_value:'yes'},
          {attr_key:'Espresso',attr_value:'yes'},
          {attr_key:'Coffee',attr_value:'yes'},
          {attr_key:'Milk coffee',attr_value:'yes'},
          {attr_key:'Cappuccino',attr_value:'yes'},
          {attr_key:'Latte Macchiato',attr_value:'yes'},
          {attr_key:'Chococcino',attr_value:'optional'},
          {attr_key:'Brewed coffee',attr_value:'optional'},
          {attr_key:'Hot water',attr_value:'yes'},
          {attr_key:'Cold milk',attr_value:'yes'},
          {attr_key:'Warm milk',attr_value:'yes'},
          {attr_key:'Chocolate',attr_value:'optional'},
          {attr_key:'Steam',attr_value:'no'},
          {attr_key:'Warm milk foam',attr_value:'yes'},
          {attr_key:'Cold milk foam',attr_value:'yes'},
          {attr_key:'Flavour shots (sirups)',attr_value:'no'}
        ],
        Capacity:[
          {attr_key:'Recommended daily output (cups)',attr_value:'80 per day'},
          {attr_key:'Recommended yearly output (cups)',attr_value:'2000'}
        ]
      },
         {
            ProductName:'A200 MS',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/223046?idFile=ZPP_001_A200-MS_nd1000w.jpg',
            ProductDescription:'<p><span>Fully automatic coffee machine with MS milk system</span><br/></p><p>2 coffee grinders</p><p>Built-in water tank or fixed water connection</p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'yes'},
              {attr_key:'Cappuccino',attr_value:'yes'},
              {attr_key:'Latte Macchiato',attr_value:'yes'},
              {attr_key:'Chococcino',attr_value:'optional'},
              {attr_key:'Brewed coffee',attr_value:'optional'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'no'},
              {attr_key:'Warm milk',attr_value:'yes'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'yes'},
              {attr_key:'Warm milk foam',attr_value:'yes'},
              {attr_key:'Cold milk foam',attr_value:'no'},
              {attr_key:'Flavour shots (sirups)',attr_value:'no'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'80 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'2000'}
            ]
          },
          {
            ProductName:'A400 NM',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/468958?idFile=ZPP_001_A400_black_nd1000w.jpg',
            ProductDescription:'<p>Fully automatic coffee machine (A400 NM - no milk)</p><p><span>1 or 2 coffee grinders; 1 or 2 powder dosing systems</span><br/></p><p><span>Built-in water tank or fixed water connection</span><br/></p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'no'},
              {attr_key:'Cappuccino',attr_value:'no'},
              {attr_key:'Latte Macchiato',attr_value:'no'},
              {attr_key:'Chococcino',attr_value:'no'},
              {attr_key:'Brewed coffee',attr_value:'optional'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'no'},
              {attr_key:'Warm milk',attr_value:'no'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'no'},
              {attr_key:'Warm milk foam',attr_value:'no'},
              {attr_key:'Cold milk foam',attr_value:'no'},
              {attr_key:'Flavour shots (sirups)',attr_value:'no'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'100 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'30000'}
            ]
          },
          {
            ProductName:'A400 MS',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/468972?idFile=ZPP_001_A400_MS-EC_black_nd1000w.jpg',
            ProductDescription:'<p>Fully automatic coffee machine with MS milk system</p><p><span>1 or 2 coffee grinders; 1 or 2 powder dosing systems</span><br/></p><p><span>Built-in water tank or fixed water connection</span><br/></p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'yes'},
              {attr_key:'Cappuccino',attr_value:'yes'},
              {attr_key:'Latte Macchiato',attr_value:'yes'},
              {attr_key:'Chococcino',attr_value:'optional'},
              {attr_key:'Brewed coffee',attr_value:'optional'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'yes'},
              {attr_key:'Warm milk',attr_value:'yes'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'optional'},
              {attr_key:'Warm milk foam',attr_value:'yes'},
              {attr_key:'Cold milk foam',attr_value:'no'},
              {attr_key:'Flavour shots (sirups)',attr_value:'no'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'100 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'2000'}
            ]
          },
          {
            ProductName:'A400 FM',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/468965?idFile=ZPP_001_A400_FM-CM_black_nd1000w.jpg',
            ProductDescription:'<p>Fully automatic coffee machine with FoamMaster milk system</p><p><span>1 or 2 coffee grinders; 1 or 2 powder dosing systems</span><br/></p><p><span>Fixed water connection</span><br/></p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'yes'},
              {attr_key:'Cappuccino',attr_value:'yes'},
              {attr_key:'Latte Macchiato',attr_value:'yes'},
              {attr_key:'Chococcino',attr_value:'optional'},
              {attr_key:'Brewed coffee',attr_value:'optional'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'yes'},
              {attr_key:'Warm milk',attr_value:'yes'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'optional'},
              {attr_key:'Warm milk foam',attr_value:'yes'},
              {attr_key:'Cold milk foam',attr_value:'yes'},
              {attr_key:'Flavour shots (sirups)',attr_value:'no'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'100 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'30000'}
            ]
          },
          {
            ProductName:'A600',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/222330?idFile=ZPP_001_A600_FM-EC_MS-EC_nd1000w.jpg',
            ProductDescription:'<p>Fully automatic coffee machine (without milk system)</p><p>1 or 2 coffee grinders;&#160;<span>1 or 2 powder dosing systems</span></p><p>Built-in water tank or fixed water connection</p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'no'},
              {attr_key:'Cappuccino',attr_value:'no'},
              {attr_key:'Latte Macchiato',attr_value:'no'},
              {attr_key:'Chococcino',attr_value:'no'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'no'},
              {attr_key:'Warm milk',attr_value:'no'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'optional'},
              {attr_key:'Warm milk foam',attr_value:'no'},
              {attr_key:'Cold milk foam',attr_value:'no'},
              {attr_key:'Flavour shots (sirups)',attr_value:'no'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'150 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'40000'}
            ]
          },
          {
            ProductName:'A600 MS EC',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/222330?idFile=ZPP_001_A600_FM-EC_MS-EC_nd1000w.jpg',
            ProductDescription:'<p>Fully automatic coffee machine with MS milk system</p><p><span>1 or 2 coffee grinders;&#160;</span><span>1 or 2 powder dosing systems</span></p><p><span>Built-in water tank or fixed water connection</span><br/></p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'yes'},
              {attr_key:'Cappuccino',attr_value:'yes'},
              {attr_key:'Latte Macchiato',attr_value:'yes'},
              {attr_key:'Chococcino',attr_value:'optional'},
              {attr_key:'Brewed coffee',attr_value:'optional'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'yes'},
              {attr_key:'Warm milk',attr_value:'yes'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'optional'},
              {attr_key:'Warm milk foam',attr_value:'yes'},
              {attr_key:'Cold milk foam',attr_value:'no'},
              {attr_key:'Flavour shots (sirups)',attr_value:'no'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'250 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'40000'}
            ]
          },
          {
            ProductName:'A600 FM CM',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/223164?idFile=ZPP_001_A600_FM-CM_nd1000w.jpg',
            ProductDescription:'<p>Fully automatic coffee machine with FoamMaster milk system</p><p>1 or 2 coffee grinders;&#160;<span>1 or 2 powder dosing systems</span></p><p><span>Fixed water connection</span><br/></p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'yes'},
              {attr_key:'Cappuccino',attr_value:'yes'},
              {attr_key:'Latte Macchiato',attr_value:'yes'},
              {attr_key:'Chococcino',attr_value:'optional'},
              {attr_key:'Brewed coffee',attr_value:'optional'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'yes'},
              {attr_key:'Warm milk',attr_value:'yes'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'optional'},
              {attr_key:'Warm milk foam',attr_value:'yes'},
              {attr_key:'Cold milk foam',attr_value:'yes'},
              {attr_key:'Flavour shots (sirups)',attr_value:'optional'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'150 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'40000'}
            ]
          },
          {
            ProductName:'A600 FM EC',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/222330?idFile=ZPP_001_A600_FM-EC_MS-EC_nd1000w.jpg',
            ProductDescription:'<p>Fully automatic coffee machine with integrated FoamMaster milk system</p><p><span>1 or 2 coffee grinders; 1 or 2 powder dosing systems</span><br/></p><p><span>Fixed water connection</span><br/></p>',
            BeverageCreations:[
              {attr_key:'Ristretto',attr_value:'yes'},
              {attr_key:'Espresso',attr_value:'yes'},
              {attr_key:'Coffee',attr_value:'yes'},
              {attr_key:'Milk coffee',attr_value:'yes'},
              {attr_key:'Cappuccino',attr_value:'yes'},
              {attr_key:'Latte Macchiato',attr_value:'yes'},
              {attr_key:'Chococcino',attr_value:'optional'},
              {attr_key:'Brewed coffee',attr_value:'optional'},
              {attr_key:'Hot water',attr_value:'yes'},
              {attr_key:'Cold milk',attr_value:'yes'},
              {attr_key:'Warm milk',attr_value:'yes'},
              {attr_key:'Chocolate',attr_value:'optional'},
              {attr_key:'Steam',attr_value:'optional'},
              {attr_key:'Warm milk foam',attr_value:'yes'},
              {attr_key:'Cold milk foam',attr_value:'yes'},
              {attr_key:'Flavour shots (sirups)',attr_value:'optional'}
            ],
            Capacity:[
              {attr_key:'Recommended daily output (cups)',attr_value:'150 per day'},
              {attr_key:'Recommended yearly output (cups)',attr_value:'40000'}
            ]
          },
          {
            ProductName:'KE200',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/223383?idFile=ZPP_001_KE200_nd1000w.jpg',
            ProductDescription:'<p>Milk storage 4 l</p>'
          },
          {
            ProductName:'UT320 CF Sinfonia',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/224408?idFile=ZPP_001_UT320_nd1000w.jpg',
            ProductDescription:'<p>Milk storage 4 l</p>'
          },
          {
            ProductName:'KE200',
            ProductImage:'https://onepim-content.franke.com/download/GetAttachmentImage/223383?idFile=ZPP_001_KE200_nd1000w.jpg',
            ProductDescription:'<p>Milk storage 4 l</p>'
          }
    ],
    PIM_Filtered:[]

};

const getters = {
    
};

const mutations = {
      setLoader(state,status){
          state.loading=status;
      },
      ADD_ITEM(state, item) {
        state.add_item = item
        console.log('state.add_item',state.add_item)
      },
      SET_PROD_SYS_DATA(state, item) {
        state.PROD_SYS = item
        console.log('state.PROD_SYS',state.PROD_SYS)
      },
      SET_flavourStation_DATA(state, item) {
        state.flavourStation = item
        console.log('state.flavourStation',state.flavourStation)
      },
      SET_FLAVOR_ST01_DATA(state, item) {
        state.FLAVOR_ST01 = item
        console.log('state.FLAVOR_ST01',state.FLAVOR_ST01)
      },
      SET_coffeeMachine_DATA(state, item) {
        state.coffeeMachine = item
        console.log('state.coffeeMachine',state.coffeeMachine)
      },
      SET_COFFEE_MC01_DATA(state, item) {
        state.COFFEE_MC01 = item
        console.log('state.COFFEE_MC01',state.COFFEE_MC01)
      },
      SET_coolingUnit_DATA(state, item) {
        state.coolingUnit = item
        console.log('state.coolingUnit',state.coolingUnit)
      },
      SET_COOLINGUNIT_DATA(state, item) {
        state.COOLINGUNIT = item
        console.log('state.COOLINGUNIT',state.COOLINGUNIT)
      },
      COFFEE_MC_Filtered_ID_Muted(state, item) {
        state.COFFEE_MC_ID = item
        console.log('state.COFFEE_MC_ID',state.COFFEE_MC_ID)
      },
      COOLINGUNIT_Filtered_ID_Muted(state, item) {
        state.COOLINGUNIT_ID = item
        console.log('state.COOLINGUNIT_ID',state.COOLINGUNIT_ID)
      },
      PROD_SYS_Filtered_Mutate(state, item) {
        state.PROD_SYS_Filtered = item
        console.log('state.PROD_SYS_Filtered',state.PROD_SYS_Filtered)
      },
      FLAVOR_ST01_Filtered_Mutate(state, item) {
        state.FLAVOR_ST01_Filtered = item
        console.log('state.FLAVOR_ST01_Filtered',state.FLAVOR_ST01_Filtered)
      },
      COFFEE_MC01_Filtered_Mutate(state, item) {
        state.COFFEE_MC01_Filtered = item
        console.log('state.COFFEE_MC01_Filtered',state.COFFEE_MC01_Filtered)
      },
      COOLINGUNIT_Filtered_Mutate(state, item) {
        state.COOLINGUNIT_Filtered = item
        console.log('state.COOLINGUNIT_Filtered',state.COOLINGUNIT_Filtered)
      },
      PIM_Filtered_Mutate(state, item) {
        state.PIM_Filtered = item
        console.log('state.PIM_Filtered',state.PIM_Filtered)
      }
};

const actions = {
    fetchData({commit}) {
      console.log('Fetching data...')
        commit('setLoader',true);
        return new Promise((resolve, reject) => {
          
        
           axios.get('api/v1/coffee/config/fetch').then(res => {
         
          resolve(res);
          console.log(res)
      })
            .catch(({response}) => {
                reject(response);
                console.log(response)
            });
        })
        .then( res => {
          
          commit('SET_PROD_SYS_DATA', res.data[10]),
          commit('SET_flavourStation_DATA', res.data[22]),
          commit('SET_FLAVOR_ST01_DATA', res.data[12]),
          commit('SET_coffeeMachine_DATA', res.data[19]),
          commit('SET_COFFEE_MC01_DATA', res.data[11]),
          commit('SET_coolingUnit_DATA', res.data[20]),
          commit('SET_COOLINGUNIT_DATA', res.data[13]),
          console.log(res.data[10]);
          
            console.log(res);
            commit('setLoader',false);
        } )
        .catch( err => {
            console.log(err);
            commit('setLoader',false);
        } );
      
    },
    addItem({ commit }, item) {
        commit('ADD_ITEM', item)
    },
    COFFEE_MC_Filtered_ID_Add({ commit }, item) {
      commit('COFFEE_MC_Filtered_ID_Muted', item)
    },
    COOLINGUNIT_Filtered_ID_Add({ commit }, item) {
      commit('COOLINGUNIT_Filtered_ID_Muted', item)
    },
    PROD_SYS_Filtered_Add({ commit }, item) {
      commit('PROD_SYS_Filtered_Mutate', item)
    },
    FLAVOR_ST01_Filtered_Add({ commit }, item) {
        commit('FLAVOR_ST01_Filtered_Mutate', item)
    },
    COFFEE_MC01_Filtered_Add({ commit }, item) {
        commit('COFFEE_MC01_Filtered_Mutate', item)
    },
    COOLINGUNIT_Filtered_Add({ commit }, item) {
        commit('COOLINGUNIT_Filtered_Mutate', item)
    },
    PIM_Filtered_Add({ commit }, item) {
        commit('PIM_Filtered_Mutate', item)
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};