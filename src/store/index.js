import Vue from "vue";
import Vuex from "vuex";
import Auth from './auth.module';
import Configuration from './configuration.module';

const state = {
    layout: 'Login'
};

const getters = {
    getLayout(state){
        return state.layout;
    }
};

const mutations = {
    setLayout(state, layout) {
        state.layout = layout
    }
};

const actions = {
    currentLayout(state) {
        return state.layout;
    }
};

Vue.use(Vuex);

// create new store

const general = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: { 
      general,
      auth: Auth,
      configuration: Configuration
  },
});

export default store;