import ApiService from '../api/api.service';
import JwtService from "../common/jwt.service";
import Errors from '../form';
import router from '../router';
import axios from "axios"; 

const getDefaultState = () => {
    return {
        test: 'hello test',
        token: null,
        errors: new Errors(),
        user: {},
        isAuthenticated: !!JwtService.getToken(),
        loading:false
    }
};

const state = getDefaultState();

const getters = {
    getErrors(state){
        return state.errors;
    },

    currentUser(state){
        return state.userId;
    },
    
    isAuthenticated(state){
        return state.isAuthenticated;
    }
};

const mutations = {
    clearErrors(state) {
        state.errors = [];
    },
    setError(state, {target, message}) {
        console.log(target,message);
        state.errors.clear();
        state.errors.record(message);
    },
    setUser(state, data) {
        state.isAuthenticated = true;
        // state.userId = data.userId;
        state.user = data.user;
        state.token = data.token;
        state.errors = getDefaultState().errors;
        JwtService.setToken(data.token);
        localStorage.setItem('userName',data.user.name);
    },
    setLoader(state,status){
        state.loading=status;
    },
    logout(state) {
        JwtService.unsetToken();
        localStorage.removeItem('userName');
        Object.assign(state, getDefaultState());
    }
};

const actions = {
    login(context, credentials) {
        context.commit('setLoader',true);
        return new Promise((resolve, reject) => {
            ApiService.post('api/v1/token', {...credentials})
            .then(({data})=>{
                resolve(data);
            })
            .catch(({response}) => {
                reject(response);
            });
        })
        .then( res => {
            console.log(res);
            context.commit('setLoader',false);
            context.commit('clearErrors');
            context.commit('setUser', {token: res.token, user: res.user });
            console.log('request sending...')
            axios.post('api/v1/coffee/config/request').then(req => {
                console.log('IdocAssign',req.data.message.IdocAssign)
            }),
            context.dispatch('configuration/fetchData', null, {root:true})
            router.push('/configurations');
        } )
        .catch( err => {
            console.log(err);
            context.commit('setLoader',false);
            context.commit('setError', {target: 'login', message: err.data.errors});
        } );
    },

    tryLogin(context, credentials){
        return new Promise((resolve, reject) => {
            var loginURL = 'api/login?email=' + credentials.email + '&password=' + credentials.password;
            ApiService.post(loginURL)
            .then(({data})=>{
                context.commit('clearErrors');
                context.commit('setUser', {token: data.success.token });
                resolve(data);
                router.push('/dashboard');
            })
            .catch(({response}) => {
                console.log(response);
                // context.commit('setError', {target: 'login', message: response.data.error});
                reject(response);
            });
        });
    },

    logout(context) {
        context.commit('logout');
        router.push('/');
        /*return new Promise((resolve, reject) => {
            ApiService.get("api/logout")
                .then(({data}) => {
                    console.log(data);
                    context.commit('logout');
                    resolve(data);
                })
                .catch(({response}) => {
                    context.commit(
                        'setError',
                        {target: 'logout', message: response.data.error}
                    );
                    reject(response);
                });
        });*/
    },

    checkAuth(context) {
        if (JwtService.getToken()) {
            ApiService.setHeader();
            ApiService.get("api/token/validate")
                // .catch(({ response }) => {
                //     context.commit('logout');
                // });
        } else {
            context.commit('logout');
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};