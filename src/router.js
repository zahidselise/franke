import Vue from 'vue';
import Router from 'vue-router';
import store from './store';
import Login from './pages/Auth/Login';
import AddNew from './pages/Configuration/Add';
import Configurations from './pages/Configuration/Configurations';
import ConfigurationMachines from './pages/Configuration/ConfigurationMachines';
import UsedConfigurations from './pages/Configuration/UsedConfigurations.vue';
import ConfigureNew from './pages/Configuration/ConfigureNew.vue';
import DeviceSummery from './pages/Configuration/DeviceSummery.vue';
import Invoice from './pages/Configuration/Invoice.vue';

Vue.use(Router)

function guardMyroute(to, from, next)
    {
        if(store.getters['auth/isAuthenticated'])
            next();
        else
            next('/login');
    }

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Login,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/add-new',
            name: 'add-new',
            beforeEnter : guardMyroute,
            component: AddNew,
        },
        {
            path: '/configurations',
            name: 'configurations',
            beforeEnter : guardMyroute,
            component: Configurations,
        },
        {
            path: '/configuration-machines',
            name: 'configuration-machines',
            beforeEnter : guardMyroute,
            component: ConfigurationMachines,
        },
        {
            path: '/used-configurations',
            name: 'UsedConfigurations',
            beforeEnter : guardMyroute,
            component: UsedConfigurations,
        },
        {
            path: '/configure-new',
            name: 'configure-new',
            beforeEnter : guardMyroute,
            component: ConfigureNew,
        },
        {
            path: '/device-summery',
            name: 'device-summery',
            beforeEnter : guardMyroute,
            component: DeviceSummery,
        },
        {
            path: '/invoice',
            name: 'invoice',
            beforeEnter : guardMyroute,
            component: Invoice,
        },
    ],
});

/*router.beforeEach((to, from, next) => {
    next();
    // Promise.all([store.dispatch('storage/checkAuth')]).then(next);

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters['authentication/isAuthenticated']) {
            next();
        } else {
            next({ name: 'login' });
        }
    } else {
		if (to.name == 'login') {
			if (store.getters['authentication/isAuthenticated']) {
				next({name: 'dashboard'});
			}
		} else {
			next();
		}
    }

});*/

export default router;